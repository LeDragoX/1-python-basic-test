# Python Basic Test

Tópicos Avançados 2: Primeira integração com CI

## Running manually the tests

```sh
python3 -m unittest discover -s "tests" -p "*_test.py" -v # Verbose testing
```

## Where are the TEST files?

**Tests needs to be under the [tests](tests/) folder.**
